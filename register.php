<?php
$gender = array(0 => "Nam", 1 => "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân sinh viên</title>
    <style>
    .signup-background {
        display: flex;
        align-items: center;
        justify-content: center;
        /* height: 20rem; */
        margin-top: 2rem
    }

    .signup {
        display: flex;
        flex-direction: column;
        width: 30rem;
        margin: auto;
        margin-top: 4rem;
        padding: 4.8rem 2.8rem 3.8rem 2.8rem;
        border: solid 2px #4e7aa3;
    }

    .signup-form {
        font-size: 18px;
        width: 100%;
        display: flex;
        margin-bottom: -1rem;
    }

    .signup-form-text {
        background-color: #5b9bd5;
        border: 2px solid #4e7aa3;
        width: 100px;
        padding: 0.8rem 0.6rem 0.2rem 0.6rem;
        margin-right: 2rem;
        text-align: center;
        color: #fff;
    }

    input[type="text"] {
        width: 60%;
        height: 2.4rem;
        margin-top: 1rem;
        padding-left: 0.8rem;
        border: 2px solid #4e7aa3;
    }

    .gender {
        display: flex;
        align-items: center;
        width: 50px;
        padding: 0px;
        margin-left: 10px;
    }

    select {
        border: 2px solid #4e7aa3;
        margin-top: 1rem;
        padding: 0px;
        outline: none;
        width: 30%;
        height: 2.8rem;
    }

    .signup-submit {
        justify-content: center;
        margin-top: 2rem;
    }

    input[type="submit"] {
        height: 2.8rem;
        width: 8rem;
        padding: 12px 30px;
        background-color: #70ad46;
        border-radius: 10px;
        border: solid 2px #477990;
        cursor: pointer;
        color: white;
    }
    </style>
</head>

<body>
    <div class="signup-background">
        <div class="signup">
            <form action="" method=" POST">
                <div class="signup-form">
                    <p class="signup-form-text">Họ và tên</p>
                    <input required name="fullName" type="text">
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">Giới tính</p>
                    <div class="gender">
                        <?php foreach ($gender as $key => $value) : ?>
                        <input required id=<?= $key ?> type="radio" name="gender" value=<?= $key ?>>
                        <label style="margin-right: 10px" for=<?= $key ?>>
                            <?= $value; ?>
                        </label>
                        <?php endforeach; ?>

                    </div>

                </div>
                <div class="signup-form">
                    <p class="signup-form-text">Phân khoa</p>
                    <select required name="department" id="department">
                        <option disabled selected value></option>
                        <?php foreach ($department as $key => $value) : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="signup-form signup-submit">
                    <input type="submit" value="Đăng Kí">
                </div>
            </form>
        </div>
    </div>

</body>

</html>